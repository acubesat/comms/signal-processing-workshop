# Signal Processing Workshop

This project contains the files for the Signal Processing with Python Workshop, organized for the recruitment of AcubeSAT, in November 2021.

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/acubesat%2Fcomms%2Fsignal-processing-workshop/main)
