from setuptools import setup

setup(
   name='acubesat-dsp-py-workshop',
   version='1.0',
   description='A collection of Python jupyter notbeooks',
   #packages=['acubesat-dsp-py-workshop'], 
   install_requires=['numpy', 'scipy', 'torchvision', 'torchaudio', 'matplotlib', 'ipython', 'ipywebrtc', 'pydub', 'torch',
   'ffmpeg', 'ffprobe', 'scikit-image', 'Soundfile'], #external packages as dependencies
)
